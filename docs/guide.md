## 배포 문제 <a id="problems"></a>
Python 프로젝트를 설정하는 것은 특히 개발자가 아닌 사람들에게 어려울 수 있다. 설정은 종종 터미널을 여는 것으로 시작하는데, 터미널은 잠재적 사용자들 그룹을 위한 것이라 할 수 없다. 이는 설치 가이드가 가상 환경, Python 버전 및 수많은 잠재적 종속성에 대한 복잡한 세부 사항을 설명하기도 전에 사용자들은 포기할 것이다.

Python 어플리케이션 개발을 위하여 시스템을 설정할 때 일반적으로 다음과 같은 과정을 거칠 것이다.

- 특정 버전의 Python 다운로드 및 설치
- pip 설정
- 가상 환경 설정
- 코드 복사본 가져오기
- 종속성 설치

만약 여러분이 Python 개발자는 말할 것도 없고 개발자가 아니라면, 잠시 멈춰서 위의 단계들 중 어떤 것을 이해하고 있는지 생각해 보면 아마 모두는 아닐 수 있을 것이다.

사용자가 다행히 설치의 종속성 부분에 도달한다 하더라도 문제가 폭발적으로 발생한다. 이것은 wheel의 보급으로 지난 몇 년 동안 잊을 수 있어 훨씬 더 좋아졌지만, 일부 의존성은 여전히 C/C++ 또는 심지어 FORTRAN 컴파일러를 필요로 한다.

가능한 한 많은 사용자가 어플리케이션을 사용할 수 있도록 하는 것이 목표라면 진입 장벽이 너무 높다. Raymond Hettinger가 그의 강연에서 종종 말하듯, "더 나은 방법이 있어야 한다."

## PyInstaller <a id="pyinstaller"></a>
PyInstaller는 모든 종속성을 찾아 함께 번들링하여 사용자로부터 이러한 세부 정보를 감춘다. Python Interpreter 자체가 당신의 어플리케이션에 번들로 제공되기 때문에 사용자들은 그들이 Python 프로젝트를 실행하고 있는지도 모를 것이다. 복잡한 설치 지침이여 안녕!

PyInstaller는 Python 코드를 검사하고 종속성을 감지한 다음 운영 체제에 따라 적합한 형식으로 패키징하여 위와 같은 기능을 수행한다.

PyInstaller에 대한 흥미로운 세부 정보가 많이 있지만, 이 튜토리얼 페이지에서는 PyInstaller의 작동 방식과 사용 방법에 대한 기본 사항을 설명한다. 자세한 내용은 [PyInstaller 문서](https://pyinstaller.readthedocs.io/en/stable/operating-mode.html#analysis-finding-the-files-your-program-needs)를 참조한다.

또한 PyInstaller는 윈도우즈, 리눅스 또는 macOS용 실행 파일을 생성할 수 있다. 즉, Windows 사용자는 .exe, Linux 사용자는 일반 실행 파일, MacOS 사용자는 .app 번들을 얻을 수 있다. 여기에는 몇 가지 주의 사항이 있으므로 자세한 내용은 [제약 사항](#limitations)을 참조한다.

## 프로젝트 준비 <a id="preparing-project"></a>
PyInstaller를 사용하려면 어플리케이션이 최소 구조, 즉 어플리케이션을 시작할 수 있는 CLI 스크립트가 있어야 한다. 이는 종종 단순히 패키지를 가져와 main()을 실행하는 Python 패키지 외부에 작은 스크립트를 생성하는 것과 같은 의미이다.

시작점(entry-point) 스크립트는 Python 스크립트이다. 시작점 스크립트에서 원하는 모든 작업을 기술적으로 수행할 수 있지만, 상대적 import를 명시적으로 사용하지 않아야 한다. 꼭 원한다면 어플리케이션 전체에 걸쳐 상대적 import를 사용할 수도 있다.

> **Note**: 시작점(entry-point)은 프로젝트 또는 어플리케이션을 시작하는 코드이다.

직접 프로젝트를 시도하거나 [Real Python feed reader 프로젝트](https://github.com/realpython/reader)를 따라 할 수 있다. 

프로젝트의 실행 파일 버전을 만드는 첫 번째 단계는 시작점 스크립트를 추가하는 것이다. 다행히 feed reader 프로젝트는 잘 구성되어 있으므로 패키지 외부에서 실행하는 데 필요한 짧은 스크립트만 있으면 된다. 예를 들어 <code>cli.py</code>이라는 파일을 다음 코드로 reader 패키지와 함께 생성할 수 있다.

```python
from reader.__main__ import main

if __name__ == '__main__':
    main()
```

<code>cli.py</code> 스크립트는 <code>main()</code>를 호출하여 feed reader를 시작한다.

코드를 잘 알고 있으므로 프로젝트를 수행할 때 시작점 스크립트를 쉽게 만들 수 있다. 그러나 다른 사람이 작성한 코드의 시작점을 찾는 것은 쉽지 않다. 이 경우 서드 파티 프로젝트내의 <code>setup.py</code> 파일에서 시작할 수 있다.

프로젝트의 <code>setup.py</code>에서 시작점 인수에 대한 참조를 찾는다. 예를 들어, reader 프로젝트의 <code>setup.py</code>설정은 다음과 같다.

```python
setup(
    name="realpython-reader",
    version="1.0.0",
    description="Read the latest Real Python tutorials",
    long_description=README,
    long_description_content_type="text/markdown",
    url="https://github.com/realpython/reader",
    author="Real Python",
    author_email="info@realpython.com",
    license="MIT",
    classifiers=[
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python",
        "Programming Language :: Python :: 2",
        "Programming Language :: Python :: 3",
    ],
    packages=["reader"],
    include_package_data=True,
    install_requires=[
        "feedparser", "html2text", "importlib_resources", "typing"
    ],
    entry_points={"console_scripts": ["realpython=reader.__main__:main"]},
)
```

위의 코드에서와 같이 <code>entry-points cli.py</code> 스크립트는 <code>entry_points</code> 인수에 언급된 것과 동일한 함수를 호출한다.

이 변경 후 reader 프로젝트 디렉토리는 다음과 같다. reader라는 폴더를 체크 아웃했다고 가정하면 다음과 같은 구조를 갖는다.

```
reader/
|
├── reader/
|   ├── __init__.py
|   ├── __main__.py
|   ├── config.cfg
|   ├── feed.py
|   └── viewer.py
|
├── cli.py
├── LICENSE
├── MANIFEST.in
├── README.md
├── setup.py
└── tests
```

reader 코드 자체에는 아무런 변화가 없으며, 단지 <code>cli.py</code>라는 새 파일만 있다. 일반적으로 이 시작점 스크립트는 PyInstaller에서 프로젝트를 사용하는 데 필요한 모든 것이다.

그러나 <code>__import__()</code>의 사용이나 함수 내부에서 import를 주의해야 한다. 이를 PyInstaller 용어로 [hidden import](https://pyinstaller.readthedocs.io/en/stable/when-things-go-wrong.html?highlight=Hidden#listing-hidden-imports)라고 한다.

어플리케이션에서 import를 변경하는 것이 너무 어려운 경우 PyInstaller가 이러한 종속성을 포함하도록 hidden import를 수동으로 지정할 수 있다. 이 방법은 튜토리얼 페이지의 뒷부분에 나온다.

패키지 외부에서 Python 스크립트를 사용하여 어플리케이션을 시작할 수 있으면 PyInstaller에서 실행 파일을 만들어 볼 수 있다.

## PyInstaller 사용법 <a id="using-pyinstaller"></a>
첫 번째 단계는 [PyPI](https://pypi.org/)에서 PyInstaller를 설치하는 것이다. 다른 Python 패키지처럼 <code>pip</code>를 사용하여 수행할 수 있다.

```shell
$ pip install pyinstaller
```

<code>pip</code>은 PyInstaller의 종속성을 새로운 명령어인 pyinstaller와 함께 설치한다. Python 코드로 PyInstaller를 import하여 라이브러리로 사용할 수 있지만 CLI 도구로만 사용할 것이다.

[자체 훅 파일을 만들면](https://pyinstaller.readthedocs.io/en/stable/hooks.html#) 라이브러리 인터페이스로 사용할 수도 있다.

순수 Python 종속성만 있는 경우 PyInstaller의 기본값으로 실행 파일을 생성할 가능성이 높다. 그러나 [C/C++ extensions](https://realpython.com/build-python-c-extension-module/)과 복잡한 종속성이 있는 경우에는 너무 많이 강조하지 말아야 한다.

PyInstaller는 사용자의 추가 작업 없이 [NumPy](http://www.numpy.org/), [PyQt](https://pypi.org/project/PyQt5/), [Matplotlib](https://matplotlib.org/)과 같이 범용적인 패키지를 지원한다. PyInstaller에서 공식적으로 지원하는 패키지 목록에 대한 자세한 내용은 [PyInstaller 문서](https://github.com/pyinstaller/pyinstaller/wiki/Supported-Packages)를 참조한다.

일부 종속 항목이 공식 문서에 나열되어 있지 않더라도 너무 걱정하지 않아도 된다. 많은 Python 패키지는 잘 작동한다. 사실, PyInstaller는 인기가 있어 많은 프로젝트들이 PyInstaller를 어떻게 작동시키는지에 대한 설명을 포함하고 있다.

간단히 말해서, 여러분의 프로젝트는 즉시 실행될 가능성이 높다.

모든 기본값으로 실행 파일을 만들려면 PyInstaller에 기본 시작점 스크립트의 이름을 지정하여야 한다.

먼저 시작점이 있는 폴더로 <code>cd</code>를 하고 PyInstaller를 설치할 때 PATH에 추가된 pyinstaller 명령에 폴더를 인수로 전달한다.

예를 들어 feed reader 프로젝트를 수행하는 경우 상위 reader 디렉터리로 cd한 후 다음을 입력한다.

```shell
$ pyinstaller cli.py
```

실행 파일을 작성하는 동안 많은 출력이 표시되더라도 무시한다. PyInstaller는 기본적으로 상세하며 디버깅을 위해 나중에 상세하게 볼 수 있도록 크랭킹할 수 있다.

## PyInstaller 아티팩트 파보기 <a id="#digging-into-pyinstall-artifacts"></a>
PyInstaller는 후드가 복잡하고 많은 출력을 생성한다. 그래서, 우선 무엇에 초점을 맞춰야 하는지 아는 것이 중요하다. 즉, 사용자에게 배포할 수 있는 실행 파일과 잠재적인 디버깅 정보이다. 기본적으로 pyinstaller 명령은 다음과 같은 몇 가지 항목을 생성한다.

- *.spec 파일
- <code>build/</code>폴더
- <code>dist/</code>폴더

### Spec 파일
기본적으로 spec 파일의 이름은 CLI 스크립트의 이름으로 부터 지정된다. 이전 예제를 그대로 사용하면 <code>cli.spec</code>이라는 파일로 표시된다. <code>cli.py</code> 파일에서 PyInstaller를 실행한 후 spec 파일의 디폴트는 다음과 같다.

```python
# -*- mode: python -*-

block_cipher = None

a = Analysis(['cli.py'],
             pathex=['/Users/realpython/pyinstaller/reader'],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          [],
          exclude_binaries=True,
          name='cli',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          console=True )
coll = COLLECT(exe,
               a.binaries,
               a.zipfiles,
               a.datas,
               strip=False,
               upx=True,
               name='cli')
```

pyinstaller 명령으로 자동으로 이 파일을 생성한다. 버전에 따라 경로는 다르지만 대부분은 동일하다.

PyInstaller를 효과적으로 사용하기 위해 위의 코드를 이해할 필요는 없다!

이 파일을 수정하여 나중에 실행 파일을 만드는 데 다시 사용할 수 있다. pyinstaller 명령에 시작점 스크립트 대신 이 스펙 파일을 제공하여 향후 빌드 속도를 높일 수도 있다.

[PyInstaller spec 파일의 몇 가지 구체적인 사용 사례](https://pyinstaller.readthedocs.io/en/stable/spec-files.html#using-spec-files)가 있다. 그러나 간단한 프로젝트의 경우 개발자가 프로젝트 구축 방법을 크게  커스터마니징하지 않는 한 이러한 세부 사항에 대해 신경 쓸 필요가 없다.

### Build 폴더
<code>build/</code>폴더는 PyInstaller가 실행 파일을 빌드하기 위한 대부분의 메타데이터와 내부 기록를 저장하는 폴더이다. 기본 내용은 다음과 같다.

```
build/
|
└── cli/
    ├── Analysis-00.toc
    ├── base_library.zip
    ├── COLLECT-00.toc
    ├── EXE-00.toc
    ├── PKG-00.pkg
    ├── PKG-00.toc
    ├── PYZ-00.pyz
    ├── PYZ-00.toc
    ├── warn-cli.txt
    └── xref-cli.html

```

<code>build</code> 폴더는 디버깅에 유용할 수 있지만 문제가 없는 경우 대부분 이 폴더를 무시한다. 이 튜토리얼 패이지의 뒷부분에서 디버깅에 대해 자세히 살펴보려 한다.

### Dist 폴더
빌드 후 다음과 유사하게 <code>dist/</code>폴더가 나타난다.

```
dist/
|
└── cli/
    └── cli
```

<code>dist/</code>폴더에는 사용자에게 전달할 최종 아티팩트를 갖고 있다. <code>dist/</code>폴더 내부에는 시작점의 이름을 딴 폴더가 있다. 이 예에서는 어플리케이션을 위한 모든 종속성과 실행 파일은 <code>dist/cli</code> 폴더에 있다. 실행할 실행 파일은 Windows에서 생성하는 경우 <code>dist/cli/cli</code> 또는 <code>dist/cli/cli.exe</code>이다.

운영 체제에 따라 확장자가 <code>.so</code>, <code>.pyd</code> 및 <code>.dll</code>인 파일도 많이 볼 수 있다. 이는  PyInstaller가 만들고 수집한 프로젝트의 종속성을 나타내는 공유 라이브러리이다.

> **Note**: 버전 제어를 위하여 git을 사용하는 경우 <code>.gitignore</code> 파일에 <code>*.spec</code>, <code>build/</code>, <code>dist/</code>를 추가하여 git 상태를 깨끗하게 유지할 수 있다. Python 프로젝트를 위한 [디폴트 GitHub .gitignore 파일](https://github.com/github/gitignore/blob/master/Python.gitignore)은 이미 이 작업을 수행한다.

전체 dist/cli 폴더를 배포하고 싶지만 적절한 이름으로 cli 이름을 변경할 수 있다.

feed reader 예제의 경우 이 시점에서 <code>dist/cli/cli</code> 실행 파일을 실행할 수 있다.

실행 파일을 실행하면 <code>version.txt</code> 파일을 언급하는 오류가 발생할 수 있다. 그 이유는 feed reader와 그 종속성에 PyInstaller가 알지 못하는 일부 추가 데이터 파일이 필요하기 때문이다. 이 문제를 해결하려면 PyInstaller에게 <code>version.txt</code>이 필요하다는 것을 알려야 한다. 이는 [새 실행파일 검사](#testing-new-executable)에서 설명할 것이다.

## Builds 커스터마이징 <a id="customizing-builds"></a>
PyInstaller에는 spec 파일 또는 일반 CLI 옵션으로 제공할 수 있는 많은 옵션이 있다. 아래에서 가장 일반적이고 유용한 옵션을 설명한다.

### --name
> 실행 파일 명을 변경

이렇게 하면 실행 파일, spec 파일 및 빌드 아티팩트 폴더의 이름이 시작점 스크립트의 이름으로 지정되는 것을 방지할 수 있다. <code>--name</code>은 시작점 스크립트의 이름을 <code>cli.py</code>으로 지정하는 습관이 있는 경우에 유용하다.

다음과 같은 명령을 사용하여 <code>cli.py</code> 스크립트에서 <code>realpython</code>이라는 실행 파일을 만들 수 있다.

```shell
$ pyinstaller cli.py --name realpython
```

### --onefile
> 어플리케이션 전체를 단일 실행 파일로 패키지화한다.

디폴트 옵션은 종속성 *및* 실행 파일의 폴더를 만드는 반면 <code>--onefile</code>은 *실행 파일만* 만들어 배포가 더 쉬워진다.

이 옵션은 인수를 사용하지 않는다. 프로젝트를 단일 파일로 번들하려면 다음과 같은 명령을 사용하여 빌드할 수 있다.

```shell
$ pyinstaller cli.py --onefile
```

위의 명령으로 <code>dist/</code> 폴더에 종속성이 있는 별도의 파일을 갖는 폴더 대신 단일 실행 파일만을 포함한다.

### --hidden-import
> PyInstaller가 자동으로 탐지하지 못한 최상위 import를 나열한다.

이것은 내부 함수 import 및 <code>__import__()</code>를 사용하여 코드를 해결하는 한 가지 방법이다. 또한 <code>--hidden-import</code>를 동일한 명령에서 여러 번 사용할 수 있다.

이 옵션을 사용하려면 실행 파일에 포함할 패키지의 이름이 필요하다. 예를 들어 프로젝트가 함수 내부에서 [requests](https://realpython.com/python-requests/) 라이브러리를 가져오는 경우 PyInstaller는 실행 파일에 requests을 자동으로 포함하지 않는다. 다음 명령을 사용하여 요청을 강제로 포함할 수 있다.

```shell
$ pyinstaller cli.py --hiddenimport=requests
```

각 hidden import마다 빌드 명령에서 이를 여러 번 지정할 수 있다.

### --add-date and --add-binary
> PyInstaller에게 빌드에 추가 데이터 또는 이진 파일을 삽입하도록 지시한다.

이 기능은 configuration 파일, examples 또는 기타 데이터를 번들로 포함하려는 경우에 유용하다. feed reader 프로젝트 경우 나중에 이 예제를 볼 수 있다.

### --exclude-module
> 실행 파일에 일부 모듈이 포함되지 않도록 제외

이는 테스트 프레임워크와 같이 개발자 전용 요구 사항을 제외하는 데 유용하다. 이는 사용자에게 제공하는 아티팩트를 최대한 작게 유지하는 좋은 방법이다. 예를 들어 <code>pytest</code>를 사용하는 경우 실행 파일에서 이 항목을 제외할 수 있다.

```shell
$ pyinstaller cli.py --exclude-module=pytest
```

### --w
> stdout 로깅을 위해 콘솔 창을 자동으로 열지 않도록 한다.

이 기능은 GUI 어플리케이션을 구축하는 경우에만 유용하다. 이렇게 하면 사용자가 터미널을 볼 수 없도록 하여 구현의 세부 정보를 숨길 수 있다.

<code>--onefile</code> 옵션과 유사하게 <code>-w</code>는 인수가 없다.

```shell
$ pyinstaller cli.py -w
```

### .spec 파일
앞에서 언급한 것처럼 자동으로 생성된 <code>.spec</code> 파일을 재사용하여 실행 파일을 추가로 커스터마이징할 수 있다. <code>.spec</code> 파일은 PyInstaller 라이브러리 API를 암시적으로 사용하는 일반 Python 스크립트이다.

일반 Python 스크립트이기 때문에 안에서 거의 모든 것을 할 수 있다. [Official PyInstaller Spec File Documentation](https://pyinstaller.readthedocs.io/en/stable/spec-files.html)에서 해당 API에 대한 자세한 내용을 찾을 수 있다.

## 새 실행파일 검사 <a id="testing-new-executable"></a>
새 실행 파일을 테스트하는 가장 좋은 방법은 새 컴퓨터에서 실행하는 것이다. 새 시스템에는 빌드 시스템과 동일한 OS가 있어야 한다. 이 시스템은 사용자가 사용하는 것과 가능한 유사해야 한다. 항상 가능하지 않으므로 차선책은 개발 컴퓨터에서 테스트하는 것이다.

핵심은 *개발 환경을 활성화하지 않고* 생성된 실행 파일을 실행하는 것이다. 즉, <code>virtualenv</code>, <code>conda</code> 또는 Python 설치에 접근할 수 있는 다른 **환경** 없이 실행한다. PyInstaller이 생성한 실행 파일의 주요 목표 중 하나는 사용자가 컴퓨터에 아무것도 설치하지 않아도 실행되는 것이다.

feed reader 예제에서 <code>dist/cli</code> 폴더에서 디폴트 <code>cli</code> 실행 파일이 실행되지 못하였다. 다행히 이 오류는 문제를 지적하였다.

```shell
FileNotFoundError: 'version.txt' resource not found in 'importlib_resources'
[15110] Failed to execute script cli
```

패키지 <code>importlib_resources</code>는 <code>version.txt</code> 파일이 필요하다. <code>--add-data</code> 옵션을 사용하여 이 파일을 추가하고 다시 빌드한다. 아래는 필요한 <code>version.txt</code>을 포함한 예이다.

```shell
$ pyinstaller cli.py \
    --add-data venv/reader/lib/python3.6/site-packages/importlib_resources/version.txt:importlib_resources
```

예는 PyInstaller에  <code>importlib_resources</code> 라는 빌드 안의 새 폴더에 <code>importlib_resources</code>에 <code>version.txt</code> 파일을 포함하라는 것이다. 

> **Note**: pyinstaller 명령에 <code>\</code> 문자를 사용하여 명령을 쉽게 읽을 수 있다. 명령을 직접 실행할 때 <code>\</code>을(를) 생략하거나, 동일한 경로를 사용하는 경우 아래 명령을 복사하여 사용할 수 있다.

feed reader의 종속성을 설치한 위치와 일치하도록 위의 명령에서 경로를 조정하고자 한다.

이제 새 실행 파일을 실행하면 <code>config.cfg</code> 파일에 대한 새 오류가 발생하였다.

이 파일은 feed reader 프로젝트에 필요하므로 빌드에 포함시켜야 한다.

```shell
$ pyinstaller cli.py \
    --add-data venv/reader/lib/python3.6/site-packages/importlib_resources/version.txt:importlib_resources \
    --add-data reader/config.cfg:reader
```

feed reader 프로젝트가 있는 위치를 기준으로 파일의 경로를 조정해야 한다.

이 때 사용자에게 직접 제공할 수 있는 실행 파일이 있어야 한다!

## PyInstaller 실행파일 디버깅 <a id="debugging=pyinstaller-debugging"></a>
위에서 처럼 실행 파일을 실행할 때도 문제가 발생할 수 있다. 프로젝트의 복잡성에 따라 디버깅이 feed reader 예제와 같이 데이터 파일을 포함하는 것만큼 간단할 수 있다. 그러나 때로는 복잡한 디버깅 기술이 필요할 수 있다.

다음은 일반적인 전략이다. 종종 이 전략 중 하나 또는 조합이 어려운 디버깅의 돌파구로 이어질 수 있다.

### Terminal 사용
먼저 모든 출력을 볼 수 있도록 터미널에서 실행 파일을 실행한다.

콘솔 창에서 모든 stdout을 보려면 <code>-w</code> 빌드 플래그를 제거해야 한다. 종속성이 누락된 경우 <code>ImportError</code> 예외가 나타나는 경우가 많다.

### 파일 디버깅
모든 경우 <code>build/cli/warn-cli.txt</code> 파일를 검사한다. PyInstaller는 PyInstaller가 생성하는 내용을 정확하게 이해할 수 있도록 도움이 되는 많은 출력을 생성한다. <code>build/</code>폴더를 주의 깊게 살펴보는 것으로 시작하도록 한다.

### 단일 디렉토리 빌드
단일 실행 파일 대신 배포 폴더를 만드는 <code>--onedir</code> 배포 모드를 사용한다. 다시 말하지만 이 모드가 디폴트 모드이다. <code>--onedir</code>로 빌드하면 단일 실행 파일에 모든 항목이 숨겨지는 대신 포함된 모든 종속성을 검사할 수 있다.

<code>--onedir</code>는 디버깅에 유용하지만<code>--onefile</code>이 일반적으로 사용자가 이해하기 쉽다. 디버깅 후 배포를 간소화하기 위해 <code>--onefile</code> 모드로 전환할 수 있다.

### 추가 CLI 옵션
PyInstaller에는 빌드 프로세스 중에 출력되는 정보의 양을 제어하는 옵션이 있다. PyInstaller에 <code>--log-level=DEBUG</code> 옵션을 사용하여 실행 파일을 다시 빌드하고 출력을 검토할 수 있다. 

PyInstaller는 <code>--log-level=DEBUG</code>에 verbosity를 높이면 많은 출력을 생성한다. 터미널에서 이를 출력하지 않고 나중에 참조할 수 있는 파일에 저장하는 것이 유용하다. 이렇게 하려면 셸의 리디렉션 기능을 사용할 수 있다. 아래는 그 예이다.

```shell
$ pyinstaller --log-level=DEBUG cli.py 2> build.txt
```

위의 명령을 사용하면 추가적인 많은 DEBUG 메시지가 포함된 <code>build.txt</code>라는 파일을 생성한다.

> **Note**: <code>></code>를 사용한 표준 리디렉션으로는 충분하지 않다. PyInstaller는 <code>stdout</code>이 아닌 <code>stderr</code> 스트림에 출력한다. 즉, <code>stderr</code> 스트림을 파일로 리디렉션해야 하며, 이전 명령에서와 같이 <code>2</code>를 사용하여 수행할 수 있다.

아래는 <code>build.txt</code>의 샘플이다. 

```
67 INFO: PyInstaller: 3.4
67 INFO: Python: 3.6.6
73 INFO: Platform: Darwin-18.2.0-x86_64-i386-64bit
74 INFO: wrote /Users/realpython/pyinstaller/reader/cli.spec
74 DEBUG: Testing for UPX ...
77 INFO: UPX is not available.
78 DEBUG: script: /Users/realptyhon/pyinstaller/reader/cli.py
78 INFO: Extending PYTHONPATH with paths
['/Users/realpython/pyinstaller/reader',
 '/Users/realpython/pyinstaller/reader']
```

이 파일에는 빌드에 포함된 항목, 포함되지 않은 항목 및 실행 파일이 패키징된 방법에 대한 자세한 정보가 많이 포함되어 있다.

더 많은 정보를 위하여 <code>--log-level</code> 옵션을 사용하는 것에 더하여 추가로 <code>--debug</code> 옵션을 사용하여 실행 파일을 다시 빌드할 수도 있다.

> **Note**: <code>-y</code> 및 <code>--clean</code> 옵션은 다시 빌드할 때 유용하다. 특히 처음 빌드를 구성할 때 또는 [연속 통합(Continuous Integration)](https://realpython.com/python-continuous-integration/)을 구축할 때 유용하다. 이러한 옵션을 예전 빌드에서 제거하면 빌드 프로세스 중에 사용자 입력이 불필요하다.

### PyInstaller 추가 문서
[PyInstaller GitHub Wiki](https://github.com/pyinstaller/pyinstaller/wiki)에 유용한 링크와 디버깅 팁이 많이 있다. 가장 주목할 만한 부분은 [모든 것이 정확하게 패키징되었는지 확인하는 부분](https://github.com/pyinstaller/pyinstaller/wiki/How-to-Report-Bugs#make-sure-everything-is-packaged-correctly)과 [잘못될 경우](https://github.com/pyinstaller/pyinstaller/wiki/If-Things-Go-Wrong) 어떻게 해야 하는지에 대한 부분입니다.

### 종속성 탐지 지원
PyInstaller가 모든 종속성을 정확히 탐지하지 못하는 경우 가장 일반적인 문제인 <code>ImportError</code> 예외가 발생한다. 앞에서 언급했듯이 <code>__import__()</code> 함수 내부에서 import 문을 사용하거나 다른 유형의 [hidden imports](https://pyinstaller.readthedocs.io/en/stable/when-things-go-wrong.html?highlight=Hidden#listing-hidden-imports)를 사용하는 경우 이 문제가 발생할 수 있다.

이러한 유형의 문제는 대부분 PyInstaller CLI의 <code>--hidden-import</code> 옵션을 사용하여 해결할 수 있다. 이는 PyInstaller에게 모듈이나 패키지를 자동으로 감지하지 않더라도 포함되도록 한다. 이 방법은 어플리케이션에서 많은 [동적 import 마법(dynamic import magic)](https://realpython.com/python-import/)을 해결하는 가장 쉬운 방법입니다.

문제를 해결하는 또 다른 방법은 [hook 파일](https://pyinstaller.readthedocs.io/en/stable/hooks.html)이다. 이 파일에는 PyInstaller가 종속성을 패키징하는 데 도움이 되는 추가 정보가 포함되어 있다. 직접 후크(hook)를 작성하고 PyInstaller에게 CLI 옵션 <code>--additional-hooks-dir</code>과 함께 사용할 수 있다.

후크 파일은 PyInstaller 자체가 내부적으로 작동하는 방식이기 때문에 [PyInstaller 소스 코드](https://github.com/pyinstaller/pyinstaller/tree/develop/PyInstaller/hooks)에서 많은 예제 후크 파일을 찾을 수 있다.

## 제약 사항 <a id="limitations"></a>
PyInstaller는 매우 강력하지만 몇 가지 제한을 갖고 있다. 시작점 스크립트의 hidden import 및 relative import에 대한 일부 제한 사항들을 이전에 기술하였다. 

PyInstaller는 Window, Linux와 macOS용 실행 파일을 만들 수 있지만 [교차 컴파일(cross compile)](https://en.wikipedia.org/wiki/Cross_compiler)은 할 수 없다. 따라서 한 운영 체제에서 다른 운영 체제를 대상으로 하는 실행 파일을 만들 수 없다. 그러므로 여러 유형의 OS에 대한 실행 파일을 배포하려면 지원되는 각 OS에 대한 빌드 시스템이 필요하다.

교차 컴파일 제한과 관련하여 PyInstaller가 어플리케이션을 실행하는 데 필요한 모든 것을 기술적으로 번들하지는 않는다는 것을 아는 것이 유용하다. 실행 파일은 여전히 사용자의 [glibc](https://en.wikipedia.org/wiki/GNU_C_Library)에 종속되어 있다. 일반적으로 대상으로 하는 각 OS의 가장 오래된 버전을 기반으로 구축하여 glibc 제한을 해결할 수 있다.

예를 들어 광범위한 Linux 시스템을 대상으로 하려면 이전 버전의 [CentOS](https://www.centos.org/)를 기반으로 구축할 수 있다. 이렇게 하면 기본 버전보다 최신 버전인 대부분의 버전과 호환된다. 이는 [PEP 0513](https://www.python.org/dev/peps/pep-0513/)에 설명하는 전략과 동일하며, [PyPA](https://www.pypa.io/en/latest/)가 호환되는 wheels를 제작할 때 권장하는 전략이다.

실제로 Linux 빌드 환경에 [PyPA의 많은 리눅스 도커 이미지](https://github.com/pypa/manylinux)를 사용할 수 있도록 해야 한다. 기본 이미지로 시작한 다음 모든 종속성과 함께 PyInstaller를 설치하고 대부분의 Linux 버전을 지원하는 빌드 이미지를 가질 수 있다.

## 끝내며 <a id="concluding-remarks"></a>
PyInstaller는 복잡한 설치 문서를 불필요하도록 한다. 대신 사용자는 실행 파일을 실행하여 최대한 빨리 시작할 수 있다. PyInstaller 워크플로는 다음과 같은 그 프로세스로 요약할 수 있다.

1. 기본 함수를 호출하는 시작점 스크립트를 생성한다.
1. PyInstaller를 설치한다.
1. 시작점에서 PyInstaller를 실행한다.
1. 새 실행 파일을 테스트한다.
1. 결과로 생성된 <code>dist/</code> 폴더를 사용자에게 배포한다.

사용자는 어떤 버전의 Python을 사용했는지 또는 어플리케이션에서 Python을 사용하는 지 전혀 알 필요가 없다!
