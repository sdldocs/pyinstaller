이 페이지는 [Using PyInstaller to Easily Distribute Python Applications](https://realpython.com/pyinstaller-python/)를 편역한 것이다.

### 목차

- [배포 문제](guide/#problems)
- [PyInstaller](guide/#pyinstaller)
- [프로젝트 준비](guide/#preparing-project)
- [PyInstaller 사용법](guide/#using-pyinstaller)
- [PyInstaller 아티팩트 파보기](guide/#digging-into-pyinstall-artifacts)
- [Builds 커스터마이징](guide/#customizing-builds)
- [새 실행파일 검사](guide/#testing-new-executable)
- [PyInstaller 실행파일 디버깅](guide/#debugging=pyinstaller-debugging)
- [제약 사항](guide/#limitations)
- [끝내며](guide/#concluding-remarks)

[Go](https://golang.org/) 개발자들이 실행 파일을 만들고 그것을 사용자에게 쉽게 전달하는 것이 부러울 것이다. **사용자가 아무 것도 설치하지 않고 어플리케이션을 실행할 수 있다면 좋지 않을까?** 그것이 바로 목표이며, [PyInstaller](https://pyinstaller.readthedocs.io/en/stable/)는 Python 생태계에서 그곳에 도달하는 한 가지 방법이다.

[가상 환경 설정](https://realpython.com/python-virtual-environments-a-primer/), [종속성 관리](https://realpython.com/courses/managing-python-dependencies/), [종속성 함정 회피](https://realpython.com/dependency-pitfalls/) 및 [PyPI에 퍼블리시](https://realpython.com/pypi-publish-python-package/)하는 방법에 대한 지침서는 많이 있다. PyPI는 Python 라이브러리를 배포할 때 유용하다. **Python 애플리케이션을 만드는 개발자**에게는 정보가 훨씬 적다. 이 페이지에서는 Python 개발자일 수도 있고 아닐 수도 있는 사용자에게 어플리케이션을 배포하려는 개발자를 위한 것이다.

이 페이지에서는 다음과 같은 것을 설명한다.

- PyInstaller가 어플리케이션 배포를 단순화하는 방법
- 프로젝트에서 PyInstaller를 사용하는 방법
- PyInstaller 오류를 디버깅하는 방법
- PyInstaller로 할 수 없는 것

PyInstaller는 사용자가 별도의 설치 없이 즉시 실행할 수 있는 폴더 또는 실행 파일을 만들 수 있는 기능을 제공한다. PyInstaller의 효용을 충분히 이해하려면 PyInstaller가 도움을 주어 해결할 수 있는 배포 문제를 살펴보는 것이 바람직할 것이다.
